import logo from "./logo.svg";
import "./App.css";
import ChooseCars from "./ChooseCars";

function App() {
  return (
    <div className="App">
      <ChooseCars />
    </div>
  );
}

export default App;
