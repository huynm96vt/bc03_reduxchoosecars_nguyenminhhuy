import React, { Component } from "react";
import { connect } from "react-redux";
import { CHANGE_CARS } from "./redux/constants/constants";

class ChooseCars extends Component {
  render() {
    return (
      <div className="container py-5">
        <div className="row">
          <div className="col-6">
            <img className="w-100" src={this.props.urlImg} alt="" />
          </div>
          <div className="py-5">
            <button
              className="btn btn-danger"
              onClick={() => {
                this.props.handleChangeCar(this.props.urlArr[0]);
              }}
            >
              Red
            </button>
            <button
              className="btn btn-dark mx-5"
              onClick={() => {
                this.props.handleChangeCar(this.props.urlArr[1]);
              }}
            >
              Black
            </button>
            <button
              className="btn btn-secondary"
              onClick={() => {
                this.props.handleChangeCar(this.props.urlArr[2]);
              }}
            >
              Silver
            </button>
          </div>
        </div>
        {this.props.urlArr.map((url) => {
          return (
            <img style={{ width: "150px", margin: "30px" }} src={url}></img>
          );
        })}
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    urlImg: state.chooseCarsReducer.urlImg,
    urlArr: state.chooseCarsReducer.urlArr,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleChangeCar: (url) => {
      dispatch({
        type: CHANGE_CARS,
        payload: url,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ChooseCars);
