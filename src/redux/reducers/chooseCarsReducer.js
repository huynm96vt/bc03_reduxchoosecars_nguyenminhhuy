import { CHANGE_CARS } from "../constants/constants";

let initialState = {
  urlImg: "./Img/cars/red-car.jpg",
  urlArr: [
    "./Img/cars/red-car.jpg",
    "./Img/cars/black-car.jpg",
    "./Img/cars/silver-car.jpg",
  ],
};

export const chooseCarsReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case CHANGE_CARS: {
      state.urlImg = payload;
      return { ...state };
    }
    default:
      return state;
  }
};
