import { combineReducers } from "redux";
import { chooseCarsReducer } from "./chooseCarsReducer";

export const rootReducer = combineReducers({ chooseCarsReducer });
